package br.com.project.payment.client;

import feign.Response;
import feign.codec.ErrorDecoder;

public class CreditCardClientDecoder implements ErrorDecoder {
    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response){
        if(response.status() == 404){
            throw new RuntimeException();
        }else{
            return errorDecoder.decode(s, response);
        }
    }
}
