package br.com.project.payment.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "CreditCard", configuration = CreditCardClientConfiguration.class)
public interface CreditCardClient {

    @GetMapping("/creditcard/checkifexists/{id}")
    boolean creditCardExistsById(@PathVariable long id);
}
