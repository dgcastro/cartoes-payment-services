package br.com.project.payment.repository;

import br.com.project.payment.model.Payment;
import org.springframework.data.repository.CrudRepository;

public interface PaymentRepository  extends CrudRepository<Payment, Long> {
    Iterable<Payment> findAllByCreditCardId(long id);
}
