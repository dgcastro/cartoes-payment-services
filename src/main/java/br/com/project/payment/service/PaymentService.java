package br.com.project.payment.service;

import br.com.project.payment.client.CreditCardClient;
import br.com.project.payment.model.Payment;
import br.com.project.payment.repository.PaymentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PaymentService {
    @Autowired
    private PaymentRepository paymentRepository;
    @Autowired
    private CreditCardClient creditCardClient;

    public Payment create(Payment payment){
        try{
            boolean creditCardExistsById = creditCardClient.creditCardExistsById(payment.getCreditCardId());
            if(creditCardExistsById){
                return paymentRepository.save(payment);
            }else{
                throw new RuntimeException("Credit Card Number not found.");
            }
        }catch(Exception exception){
            throw  new RuntimeException("Credit Card Service error.");
        }
    }

    public Iterable<Payment> findAllByCreditCardId(long creditCardId){
         return paymentRepository.findAllByCreditCardId(creditCardId);
    }
}
