package br.com.project.payment.service;

import br.com.project.payment.client.CreditCardClient;
import br.com.project.payment.model.Payment;
import br.com.project.payment.repository.PaymentRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.lang.reflect.Array;
import java.util.Arrays;

@SpringBootTest
public class PaymentServiceTest {
    @MockBean
    private PaymentRepository paymentRepository;

    @Autowired
    private PaymentService paymentService;

    @MockBean
    private CreditCardClient creditCardClient;

    private Payment payment;

    @BeforeEach
    public void setUp(){
        payment = new Payment();
        payment.setId(1);
        payment.setCreditCardId(1);
        payment.setDescription("whatever");
        payment.setValue(44.44);
    }

    @Test
    public void createPaymentTest(){
        Mockito.when(paymentRepository.save(Mockito.any(Payment.class))).thenReturn(payment);
        Mockito.when(creditCardClient.creditCardExistsById(payment.getCreditCardId())).thenReturn(true);
        Payment paymentCreateTest = paymentService.create(payment);
        Assertions.assertEquals(payment, paymentCreateTest);
    }

    @Test
    public void findPaymentsTest(){
        Iterable<Payment> payments = Arrays.asList(payment);
        Mockito.when(paymentRepository.findAllByCreditCardId(Mockito.anyLong())).thenReturn(payments);
        Iterable<Payment> paymentIterable = paymentService.findAllByCreditCardId(payment.getCreditCardId());
        Assertions.assertEquals(payments, paymentIterable);
    }
}
